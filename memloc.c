#include <stddef.h>
#include <stdbool.h>
#include <bits/types/FILE.h>
#include <stdio.h>
#include <sys/mman.h>

struct mem {
    struct mem *next;
    struct mem *prev;
    void *data;
    size_t capacity;
    bool is_free;
} init;

void *data_addr;

void *mem_addr = &init + sizeof(struct mem);

const int PROT_ALL = PROT_WRITE | PROT_READ;
const int FLAGS = MAP_PRIVATE | MAP_ANONYMOUS;

void *common_mmap(void *ptr, size_t query) {
    return mmap(ptr, query, PROT_ALL, FLAGS, -1, 0);
}

void *alloc_mem(size_t query) {
    struct mem *result = common_mmap(mem_addr, sizeof(struct mem));
    result->capacity = query;
    result->is_free = false;
    mem_addr += sizeof(struct mem);
    result->data = common_mmap(data_addr, query);
    data_addr += query;
    return result;
}

struct mem *cut_mem(struct mem *curr, size_t query) {
    struct mem *second = common_mmap(mem_addr, sizeof(struct mem));
    mem_addr += sizeof(struct mem);
    second->next = curr->next;
    second->prev = curr;
    second->is_free = true;
    curr->next = second;
    second->capacity = curr->capacity - query;
    second->data = common_mmap(data_addr, curr->capacity - query);
    curr->capacity = query;
    curr->is_free = false;
    return curr;
}

struct mem *get_free_mem(size_t query) {
    struct mem *curr = &init;
    while (curr) {
        if (curr->is_free && curr->capacity >= query) {
            if (curr->capacity > query) {
                return cut_mem(curr, query);
            } else {
                return curr;
            }
        }
        if (!curr->next) {
            curr->next = alloc_mem(query);
            curr->next->prev = curr;
//            curr->is_free = false;
            return curr->next;
        } else {
            curr = curr->next;
        }
    }
    return curr;
}


void *heap_init(size_t initial_size) {
    if (init.data) {
        munmap(init.data, init.capacity);
    }
    init.capacity = initial_size;
    init.data = common_mmap(((void *) 0x04040000), initial_size);
    init.is_free = true;
    data_addr = &init.data + initial_size;
    return init.data;
}

void *memloc(size_t query) {
    return get_free_mem(query)->data;
}

void mem_free(void *ptr) {
    struct mem *found = &init;
    while (found) {
        if (found->data == ptr) {
            break;
        }
        found = found->next;
    }
    munmap(found->data, found->capacity);
    if (found->prev) {
        found->prev->next = found->next;
    }
    if (found->next) {
        found->next->prev = found->prev;
    }
    if (found == &init) {
        init = *found->next;
    }
    munmap(found, sizeof(struct mem));
}

void memloc_debug_struct_info(FILE *f, struct mem const *const address) {
//    size_t i;
    fprintf(f, "start: %p\nsize: %lu\nis_free: %d\n", (void *) address, address->capacity, address->is_free);
//    for (i = 0; i < 4 && i < address->capacity; ++i) {
        fprintf(f, "%s", ((char *) address->data)/*[sizeof(struct mem) + i]*/);
//    }
    putc('\n', f);
}

void memloc_debug_heap(FILE *f, struct mem const *ptr) {
    for (; ptr; ptr = ptr->next)
        memloc_debug_struct_info(f, ptr);
}

int main() {
    heap_init(500);
    void *ptr30 = memloc(30);
    sprintf(ptr30, "%s", "30bytes");
    void *ptr20 = memloc(20);
    sprintf(ptr20, "%s", "20bytes");
    void *ptr10 = memloc(10);
    sprintf(ptr10, "%s", "10bytes");
    memloc_debug_heap(stdout, &init);
    mem_free(ptr20);

    mem_free(ptr30);
    mem_free(ptr10);
    memloc(450);
    memloc_debug_heap(stdout, &init);

}