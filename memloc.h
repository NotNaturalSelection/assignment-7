#ifndef _MEM_H_
#define _MEM_H_
#define _USE_MISC

#include <stddef.h>
#include  <stdint.h>
#include <stdio.h>
#include <sys/mman.h>

#define  HEAP_START  ((void*)0x04040000)
struct mem;
#pragma pack(push, 1)
struct mem {
    struct mem *next;
    size_t capacity;
    bool is_free;
};
#pragma pack(pop)

void *memloc(size_t query);

void mem_free(void *mem);

void *heap_init(size_t initial_size);

#define DEBUG_FIRST_BYTES 4

void memloc_debug_struct_info(FILE *f, struct mem const *const address);

void memloc_debug_heap(FILE *f, struct mem const *ptr);

#endif